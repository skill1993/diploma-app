//
//  TaskEntity+CoreDataProperties.swift
//  Diploma App
//
//  Created by Никита Журавлев on 20/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//
//

import Foundation
import CoreData


extension TaskEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TaskEntity> {
        return NSFetchRequest<TaskEntity>(entityName: "TaskEntity")
    }

    @NSManaged public var adress: String?
    @NSManaged public var completionDate: String?
    @NSManaged public var describe: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longtitude: Double
    @NSManaged public var name: String?
    @NSManaged public var reminder: String?
    @NSManaged public var status: Bool
    @NSManaged public var identifierOfTask: String?

}
