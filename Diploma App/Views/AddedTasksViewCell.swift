//
//  AddedTasksViewCell.swift
//  Diploma App
//
//  Created by Никита Журавлев on 15/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit

class AddedTasksViewCell: UITableViewCell {
    
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var topDataView: UIView!
    @IBOutlet weak var taskNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var dateComplLbl: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    let chechedImg = UIImage(named: "checked")
    let unchImg = UIImage(named: "unchecked")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dataView.layer.cornerRadius = 8
        topDataView.layer.cornerRadius = 8
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        
    }
    
    func configureCell(for task: TaskEntity){
        taskNameLbl.text = task.name
        descriptionLbl.text = task.describe
        if task.completionDate != nil {
            dateComplLbl.text = task.completionDate
        } else {
            dateComplLbl.text = "Дата/время не заданы"
        }
        if task.status == false {
            checkButton.setImage(unchImg, for: .normal)
        } else if task.status == true {
            checkButton.setImage(chechedImg, for: .normal)
        }
    }

}
