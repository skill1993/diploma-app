//
//  ChoosePlaceOnMapViewController.swift
//  Diploma App
//
//  Created by Никита Журавлев on 16/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class ChoosePlaceOnMapViewController: UIViewController, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var adressView: UIView!
    @IBOutlet weak var adressLabel: UILabel!
    
    var adress: String = ""
    
    var pin = MKPointAnnotation()
    
    var latDouble: Double = 0.0
    var longDouble: Double = 0.0
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var savePlaceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.removeAnnotation(pin)
        
        setMap()

        savePlaceButton.layer.cornerRadius = 8
        adressView.layer.cornerRadius = 8
        // Do any additional setup after loading the view.
    }
    
    func setMap() {
        let tgr = UITapGestureRecognizer(target: self,
                                         action: #selector(ChoosePlaceOnMapViewController.tapHandler(gestureRecognizer:)))
        tgr.delegate = self
        self.map.addGestureRecognizer(tgr)
    }
    
    @objc func tapHandler(gestureRecognizer: UITapGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            self.map.removeAnnotation(pin)
            let touchLocation = gestureRecognizer.location(in: map)
            let touchCoordinate = map.convert(touchLocation, toCoordinateFrom: map)
            latDouble = touchCoordinate.latitude
            longDouble = touchCoordinate.longitude
            
            self.pin.coordinate = CLLocationCoordinate2D(latitude: latDouble, longitude: longDouble)
            self.pin.title = "Выбранная точка"
            
            let location = CLLocation(latitude: latDouble, longitude: longDouble)
            
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
                guard let self = self else { return }
                
                if let _ = error {
                    return
                }
                
                guard let placemark = placemarks?.first else {
                    return
                }
                
                //let countyName = placemark.country ?? ""
                //let countryCode = placemark.isoCountryCode ?? ""
                let cityName = placemark.subAdministrativeArea ?? ""
                let streetNumber = placemark.subThoroughfare ?? ""
                let streetName = placemark.thoroughfare ?? ""
                
                DispatchQueue.main.async {
                    self.adress = "\(cityName), \(streetName), \(streetNumber)"
                    self.adressLabel.text = self.adress
                }
            }

            self.map.addAnnotation(pin)
            return
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
