//
//  AddNewTaskViewController.swift
//  Diploma App
//
//  Created by Никита Журавлев on 16/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import CoreData
import UIKit

class AddNewTaskViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    @IBOutlet weak var viewsSet: UIView!
    @IBOutlet weak var taskNameView: UIView!
    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var toDoToTimeView: UIView!
    @IBOutlet weak var toDoToTimeLabel: UITextField!
    
    @IBOutlet weak var toDoToTimeChangeButton: UIButton!
    @IBOutlet weak var toRememberInView: UIView!
    @IBOutlet weak var toRememerInLabel: UITextField!
    
    @IBOutlet weak var toRememberInChangeButton: UIButton!
    @IBOutlet weak var taskCompletionView: UIView!
    @IBOutlet weak var taskCompletionSwitch: UISwitch!
    @IBOutlet weak var taskDescriptionView: UIView!
    @IBOutlet weak var taskDescriptionTextView: UITextView!
    @IBOutlet weak var saveNewTaskButton: UIButton!
    @IBOutlet weak var geoTagView: UIView!
    @IBOutlet weak var geoTagLabel: UILabel!
    @IBOutlet weak var geoTagChooseButton: UIButton!

    
    // Блок переменных которыми будут оперировать для создания новой задач
    
    var task = TaskEntity()
    var nameOfTask: String = ""
    var isItDone: Bool = false
    var taskDescription: String = ""
    var toDoItTo: String = ""
    var toRememberIn: String = ""
    var adress = ""
    var longtitude = 0.0
    var latitude = 0.0
    
    private var datePicker: UIDatePicker?
    private var picker: UIPickerView?
    
    private let pickerData: [String] = ["5 мин",
                                        "10 мин",
                                        "15 мин",
                                        "20 мин",
                                        "25 мин",
                                        "30 мин",
                                        "45 мин",
                                        "1 час",
                                        "2 часа",
                                        "3 часа",
                                        "4 часа",
                                        "6 часов",
                                        "12 часов",
                                        "1 день",
                                        "2 дня"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskNameTextField.delegate = self
        taskNameTextField.text = nil
        taskNameTextField.placeholder = "Новая задача"
        
        toDoToTimeLabel.delegate = self
        toDoToTimeLabel.text = nil
        
        toRememerInLabel.delegate = self
        toRememerInLabel.text = nil
        
        taskCompletionSwitch.isOn = false
        taskDescriptionTextView.delegate = self
        taskDescriptionTextView.text = "Описание"
        taskDescriptionTextView.textColor = .lightGray
        
        geoTagLabel.text = "Выберете место"
        
        configureRoundedView()
        
        picker = UIPickerView()
    
        self.picker?.delegate = self
        self.picker?.dataSource = self
        
        toRememerInLabel.inputView = picker
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .dateAndTime
        datePicker?.addTarget(self,
                              action: #selector(dateChanged(datePicker:)),
                              for: .valueChanged)
        
        toDoToTimeLabel.inputView = datePicker
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        
        view.addGestureRecognizer(tapGesture)
        
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    
    
    fileprivate func configureRoundedView(){
        taskNameView.layer.cornerRadius = 8
        toDoToTimeView.layer.cornerRadius = 8
        toRememberInView.layer.cornerRadius = 8
        taskCompletionView.layer.cornerRadius = 8
        taskDescriptionView.layer.cornerRadius = 8
        geoTagView.layer.cornerRadius = 8
        saveNewTaskButton.layer.cornerRadius = 8
    }
    
    @IBAction func completionSwitchChange(_ sender: Any) {
        if taskCompletionSwitch.isOn == false {
            isItDone = false
        } else if taskCompletionSwitch.isOn == true {
            isItDone = true
        }
        print(isItDone)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField{
        case taskNameTextField:
            nameOfTask = taskNameTextField.text!
            print(nameOfTask)
        case toDoToTimeLabel:
            toDoItTo = toDoToTimeLabel.text!
            print(toDoItTo)
        case toRememerInLabel:
            toRememberIn = toRememerInLabel.text!
            print(toRememberIn)
        default: break
        }

        self.view.endEditing(true)
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            taskDescriptionTextView.resignFirstResponder()
            taskDescription = taskDescriptionTextView.text!
            print(taskDescription)
            return false
        } else {
            return true
        }
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (taskDescriptionTextView.text == "Описание") {
            taskDescriptionTextView.text = ""
            taskDescriptionTextView.textColor = .black
        }
        taskDescriptionTextView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (taskDescriptionTextView.text == ""){
            taskDescriptionTextView.text = "Описание"
            taskDescriptionTextView.textColor = .lightGray
        }
        taskDescriptionTextView.resignFirstResponder()
    }
    
    
    func saveTask() {
        if (taskDescription != "" && nameOfTask != "") {
            addNewTask(with: nameOfTask, describe: taskDescription, status: isItDone)
        }
    }
    
    @IBAction func datePicker(_ sender: Any) {
        toDoToTimeLabel.becomeFirstResponder()
    }
    
    @IBAction func pickerCustom(_ sender: Any) {
        toRememerInLabel.becomeFirstResponder()
    }
    
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d MMMM H:mm"
        
        toDoToTimeLabel.text = dateFormatter.string(from: datePicker.date)
        toDoItTo = toDoToTimeLabel.text!
        print(toDoItTo)
        
        view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        toRememerInLabel.text = pickerData[row]
        toRememberIn = toRememerInLabel.text!
        print(toRememberIn)
    }
    
    func addNewTask(with name: String,
                    describe: String,
                    status: Bool) {
        
        let app = UIApplication.shared.delegate as? AppDelegate
        guard let context = app?.coreDataStack.managedContext else { return }
        
        let addingTask = TaskEntity(context: context)
        addingTask.identifierOfTask = UUID().uuidString
        addingTask.name = name
        addingTask.describe = describe
        addingTask.status = status
        
        // Дальше блок данных которй может быть не заполнен
        if adress == "" {
            addingTask.adress = "Адрес не задан"
        } else {
            addingTask.adress = adress
        }
        
        if longtitude == 0.0 {
            addingTask.longtitude = 0.0
        } else {
            addingTask.longtitude = longtitude
        }
        
        if latitude == 0.0 {
            addingTask.latitude = 0.0
        } else {
            addingTask.latitude = latitude
        }
        
        if toDoItTo == "" {
            addingTask.completionDate = "Дата не задана"
        } else {
            addingTask.completionDate = toDoItTo
        }
        
        if toRememberIn == "" {
            addingTask.reminder = "Время напоминания не задано"
        } else {
            addingTask.reminder = toRememberIn
        }
        
        task = addingTask
        
        do {
            try context.save()
        } catch _ {
            print("Произошла ошибка при сохранении новой задачи")
        }
        
    }
    
    @IBAction func backFromMapVCWithSavingPlace(sender: UIStoryboardSegue){
        if let mapVC = sender.source as? ChoosePlaceOnMapViewController {
            adress = mapVC.adress
            geoTagLabel.text = adress
            latitude = mapVC.latDouble
            longtitude = mapVC.longDouble
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
