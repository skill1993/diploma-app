//
//  TasksTableViewController.swift
//  Diploma App
//
//  Created by Никита Журавлев on 16/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import CoreData
import UIKit

class TasksTableViewController: UITableViewController {
    
    @IBOutlet weak var taskCompletionSC: UISegmentedControl!
    
    var tasks = [TaskModel]()
    
    var newTasks: [TaskEntity] = []
    
    var tasksToShow: [TaskEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 128
        tableView.contentInset.top = 16
        tableView.dataSource = self
        tableView.delegate = self
        
        fetchTasksForSegment()
        
        self.taskCompletionSC.selectedSegmentIndex = 0
        
        switchSegment()
        
        self.taskCompletionSC.addTarget(self,
                                        action: #selector(selectedSegment),
                                        for: .valueChanged)

        self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    private func fetchTasksForSegment(){
        let taskRequest: NSFetchRequest<TaskEntity> = TaskEntity.fetchRequest()
        let app = UIApplication.shared.delegate as? AppDelegate
        let taskResult = try! app!.coreDataStack.managedContext.fetch(taskRequest)
        newTasks = taskResult
    }

    // MARK: - Table view data source

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    */
    
    @objc func selectedSegment(target: UISegmentedControl) {
        if target == self.taskCompletionSC {
            switchSegment()
        }
    }
    
    private func switchSegment() {
        switch taskCompletionSC.selectedSegmentIndex{
        case 0: handleUndoneTask()
        case 1: handleDoneTask()
        default: break
        }
    }
    
    private func handleUndoneTask() {
        tasksToShow = []
        for task in newTasks {
            if task.status == false {
                tasksToShow.append(task)
            }
        }
        self.tableView.reloadData()
    }
    
    private func handleDoneTask() {
        tasksToShow = []
        for task in newTasks {
            if task.status == true {
                tasksToShow.append(task)
            }
        }
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tasksToShow.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addedTasks", for: indexPath)
        
        guard let customCell = cell as? AddedTasksViewCell else {
            return cell
        }
        
        customCell.configureCell(for: tasksToShow[indexPath.row])
        
        return customCell
    }
    
    
    @IBAction func backFromAddNewTaskVC(sender: UIStoryboardSegue){
        
    }
    
    @IBAction func backFromAddNewTaskVCBySave(sender: UIStoryboardSegue){
        
        
        if let newTaskVC = sender.source as? AddNewTaskViewController {
            newTaskVC.saveTask()
            newTasks.append(newTaskVC.task)
            switchSegment()
        }
        self.tableView.reloadData()
    }

    


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let objUUID = tasksToShow[indexPath.row].identifierOfTask!
        
        let myIndexPath = [indexPath]
        // Delete the row from the data source
        deleteRecordOnUUID(object: objUUID)
        self.tasksToShow.remove(at: indexPath.row)
        self.tableView.deleteRows(at: myIndexPath, with: .fade)
        self.perform(#selector(reloadTable), with: nil, afterDelay: 2)
    }
    
    func deleteRecordOnUUID(object: String) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.coreDataStack.managedContext
        
        let deleteFetch: NSFetchRequest<TaskEntity> = TaskEntity.fetchRequest()
        deleteFetch.returnsObjectsAsFaults = false
        deleteFetch.predicate = NSPredicate(format: "identifierOfTask = %@", object)
        
        do {
            let allTasksObj = try context.fetch(deleteFetch)
            for object in allTasksObj as [NSManagedObject] {
                context.delete(object)
            }
        } catch {
            print("Error while deleting")
        }
        
        do {
            try context.save()
        } catch {
            print("Error while saving after delete")
        }
        
    }
    
    @objc func reloadTable() {
        DispatchQueue.main.async{
            self.tableView.reloadData()
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
