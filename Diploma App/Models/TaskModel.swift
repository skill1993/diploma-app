//
//  TaskModel.swift
//  Diploma App
//
//  Created by Никита Журавлев on 17/02/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import Foundation

class TaskModel {
    var taskName: String
    var timeToDo: String?
    var rememberTo: String?
    var completed: Bool
    var description: String
    var geoLocationLatitude: Double?
    var geoLocationLongtitude: Double?
    
    init(taskName: String,
         completed: Bool,
         description: String){
        self.taskName = taskName
        self.completed = completed
        self.description = description
    }
    
}

extension TaskModel: CustomStringConvertible {
    var descriptionOfClass: String {
        return """
        Task name: \(taskName)
        Time it must be done: \(timeToDo ?? "NIL")
        Remember: \(rememberTo ?? "NIL")
        Completed: \(completed)
        Description: \(description)
        Lat: \(geoLocationLatitude ?? 0.0)
        Long: \(geoLocationLongtitude ?? 0.0)
        """
    }
}
